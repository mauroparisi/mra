package tdd.training.mra;

import java.util.List;

public class MarsRover {
private int[][] pianeta ;

String ob="";
String ob1="";
String n="";

	/**
	 * It initializes the rover at the coordinates (0,0), facing North, on a planet
	 * (represented as a grid with x and y coordinates) containing obstacles.
	 * 
	 * @param planetX         The x dimension of the planet.
	 * @param planetY         The y dimension of the planet.
	 * @param planetObstacles The obstacles on the planet. Each obstacle is a string
	 *                        (without white spaces) formatted as follows:
	 *                        "(oi_x,oi_y)". <code>null</code> if the planet does
	 *                        not contain obstacles.
	 * 
	 * @throws MarsRoverException
	 */
	public MarsRover(int planetX, int planetY, List<String> planetObstacles){
		// To be implemented
		String s="";
		pianeta = new int [planetX][planetY];
        ob=planetObstacles.get(0);
        ob1=planetObstacles.get(1);
		for(int i=0;i<3;i++) {
			for(int j=0; j<3 ;j++) {
				pianeta [i][j] = 0;	
				
					
				
				if(planetContainsObstacleAt(i,j));
					
				
				
				
			}
		}
		
		
		pianeta [0][0]=2;
		pianeta [posizione(ob,0)][posizione(ob1,1)]=1;
		pianeta [2][2]=1;
		
	}
	private int posizione (String oo, int j) {
		String intero1 = oo.replaceAll("[^0-9]","");
		return Integer.parseInt(intero1.charAt(j)+"");
		
				
			
		}
	

	/**
	 * It returns whether, or not, the planet (where the rover moves) contains an
	 * obstacle in a cell.
	 * 
	 * @param x The x coordinate of the cell
	 * @param y The y coordinate of the cell
	 * @return <true> if the cell contains an obstacle, <false> otherwise.
	 * @throws MarsRoverException
	 */
	public boolean planetContainsObstacleAt(int x, int y) {
		// To be implemented
		
		return pianeta[x][y] == 1;
		
			}

	/**
	 * It lets the rover move on the planet according to a command string. The
	 * return string contains the new position of the rover, its direction, and the
	 * obstacles it has encountered while moving on the planet (if any).
	 * 
	 * @param commandString A string that can contain a single command -- i.e. "f"
	 *                      (forward), "b" (backward), "l" (left), or "r" (right) --
	 *                      or a combination of single commands.
	 * @return The return string that contains the position and direction of the
	 *         rover, and the obstacles the rover has encountered while moving on
	 *         the planet (if any). The return string (without white spaces) has the
	 *         following format: "(x,y,dir)(o1_x,o1_y)(o2_x,o2_y)...(on_x,on_y)". x
	 *         and y define the new position of the rover while dir represents its
	 *         direction (i.e., N, S, W, or E). Finally, oi_x and oi_y are the
	 *         coordinates of the i-th encountered obstacle.
	 * @throws MarsRoverException
	 */
	public String executeCommand(String commandString) {
		// To be implemented
		String result="N";
		int x=0;
		int y=0;
		
		 if(pianeta[x][y]==2) {
			 
		
		if(commandString=="" || commandString==null) { 
			result="N";
			     }else {
			if(commandString=="l") {
				result="W";}
			if(commandString=="r") {
				result="E";}
			
		} }
		 
		 if(commandString=="f") {
			 y++;
			  }else {
				  if(commandString=="b") {
					  y--;
				  }
			  }
					return "("+x+","+y+","+result+")";
					
		
	

}
	}
