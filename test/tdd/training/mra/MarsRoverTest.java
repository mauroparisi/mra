package tdd.training.mra;

import static org.junit.Assert.*;
import java.util.*;

import org.junit.Test;

public class MarsRoverTest {

	@Test
	public void marscreating() throws MarsRoverException {
		List<String> planetObstacles = new ArrayList<>();
		planetObstacles.add("(0,1)");
		planetObstacles.add("(2,2)");
		MarsRover rover = new MarsRover(3, 3, planetObstacles);
		boolean obstacle = rover.planetContainsObstacleAt(2, 2);
		assertTrue(obstacle);
	}
	@Test
	public void mrsposition() throws MarsRoverException {
		List<String> planetObstacles = new ArrayList<>();
		planetObstacles.add("(0,1)");
		planetObstacles.add("(2,2)");
		MarsRover rover = new MarsRover(3, 3, planetObstacles);
		boolean obstacle = rover.planetContainsObstacleAt(2, 2);
		String commandString = "";
		String returnString = rover.executeCommand(commandString);
		assertEquals("(0,0,N)",rover.executeCommand(""));
	}
	@Test
	public void mrspositionwhitnull() throws MarsRoverException {
		List<String> planetObstacles = new ArrayList<>();
		planetObstacles.add("(0,1)");
		planetObstacles.add("(2,2)");
		MarsRover rover = new MarsRover(3, 3, planetObstacles);
		boolean obstacle = rover.planetContainsObstacleAt(2, 2);
		String commandString = null;
		String returnString = rover.executeCommand(commandString);
		assertEquals("(0,0,N)",rover.executeCommand(null));
	}
	@Test
	public void mrspositionwhitleft() throws MarsRoverException {
		List<String> planetObstacles = new ArrayList<>();
		planetObstacles.add("(0,1)");
		planetObstacles.add("(2,2)");
		MarsRover rover = new MarsRover(3, 3, planetObstacles);
		boolean obstacle = rover.planetContainsObstacleAt(2, 2);
		String commandString = "l";
		String returnString = rover.executeCommand(commandString);
		assertEquals("(0,0,W)",rover.executeCommand("l"));
	}
	@Test
	public void mrspositionwhitright() throws MarsRoverException {
		List<String> planetObstacles = new ArrayList<>();
		planetObstacles.add("(0,1)");
		planetObstacles.add("(2,2)");
		MarsRover rover = new MarsRover(3, 3, planetObstacles);
		boolean obstacle = rover.planetContainsObstacleAt(2, 2);
		String commandString = "r";
		String returnString = rover.executeCommand(commandString);
		assertEquals("(0,0,E)",rover.executeCommand("r"));
	}
	@Test
	public void mrspositionwhitmoveup() throws MarsRoverException {
		List<String> planetObstacles = new ArrayList<>();
		planetObstacles.add("(0,1)");
		planetObstacles.add("(2,2)");
		MarsRover rover = new MarsRover(3, 3, planetObstacles);
		boolean obstacle = rover.planetContainsObstacleAt(2, 2);
		String commandString = "f";
		String returnString = rover.executeCommand(commandString);
		assertEquals("(0,1,N)",rover.executeCommand("f"));
	}
	@Test
	public void mrspositionwhitmovedown() throws MarsRoverException {
		List<String> planetObstacles = new ArrayList<>();
		planetObstacles.add("(0,1)");
		planetObstacles.add("(2,2)");
		MarsRover rover = new MarsRover(3, 3, planetObstacles);
		boolean obstacle = rover.planetContainsObstacleAt(2, 2);
		String commandString = "b";
		String returnString = rover.executeCommand(commandString);
		assertEquals("(0,-1,N)",rover.executeCommand("b"));
	}


}
